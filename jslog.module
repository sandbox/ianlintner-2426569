<?php

/**
 * Implements hook_permission().
 */
function jslog_permission() {
  return array(
    'administer jslog settings' => array(
      'title' => t('Access/Edit jslog settings'),
    )
  );
}

/**
 * Implements hook_menu()
 *
 * @return array
 */
function jslog_menu() {
  $items = array();

  $items['admin/config/development/jslog'] = array(
    'title'            => 'jslog module settings',
    'description'      => 'jslog module settings',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('jslog_settings'),
    'access arguments' => array('administer jslog settings'),
    'file'             => 'jslog.admin.inc',
    'type'             => MENU_NORMAL_ITEM,
  );

  $items['ajax/json/jslog'] = array(
    'title'           => 'Ajax log',
    'page callback'   => 'jslog_ajax_log',
    'access callback' => TRUE,
    'type'            => MENU_CALLBACK
  );

  return $items;
}

/**
 * page_callback to be used ajax callback for jslog to send log messages.
 */
function jslog_ajax_log() {
  $jslog_enabled = variable_get('jslog_enabled', TRUE);
  $jslog_identity = variable_get('jslog_identity', 'jslog');
  if ($jslog_enabled) {
    $severity = $_POST['severity'];
    $message = $_POST['message'];
    $exception = $_POST['exception'];
    $location = $_POST['location'];
    $browser_debug = $_POST['browser_debug'];
    $data = $_POST['data'];
    $parameters = array();
    if (!empty($severity) && !empty($message)) {
      if (isset($exception)) {
        $message .= ' exception: %exception';
        $parameters['%exception'] = print_r($exception, TRUE);
      }
      if (isset($data)) {
        $message .= ' data: %data';
        $parameters['%data'] = print_r($data, TRUE);
      }
      if (!empty($location)) {
        $message .= ' location: %location';
        $parameters['%location'] = $location;
      }
      if(isset($browser_debug)) {
        $message .= ' browser debug: %browser_debug';
        $parameters['%browser_debug'] = print_r($browser_debug,true);
      }
      watchdog($jslog_identity, $message, $parameters, $severity);
    }
  }
}

/**
 * Implements hook_page_build
 *
 * @param $page
 */
function jslog_page_build(&$page) {
  //Include logger on every page
  $jslog_enabled = variable_get('jslog_enabled', TRUE);
  if ($jslog_enabled) {
    //It is too early in the request stream for Drupal.Settings
    $jslog_settings = "var jslogWindowOnError = '" . variable_get('jslog_window_on_error', TRUE) . "'; ";
    $jslog_settings .= "var jslogContinue = '" . variable_get('jslog_continue', TRUE) . "'; ";
    $jslog_settings .= "var jslogConsole = '" . variable_get('jslog_console', FALSE) . "'; ";
    $jslog_settings .= "var jslogBrowserDebugData = '" .    variable_get('jslog_browser_debug_data', FALSE) . "'; ";

    //send settings inline before logging kicks off
    drupal_add_js($jslog_settings, array(
        'type'        => 'inline',
        'group'       => JS_LIBRARY,
        'weight'      => -1000,
        'every_page'  => TRUE,
        'cache'       => TRUE,
        'scope'       => 'header'
      )
    );
    drupal_add_js(drupal_get_path('module', 'jslog') . '/js/jslog.js', array(
        'group'       => JS_LIBRARY,
        'weight'      => -999,
        'every_page'  => TRUE,
        'cache'       => TRUE,
        'scope'       => 'header'
      )
    );
  }
}

